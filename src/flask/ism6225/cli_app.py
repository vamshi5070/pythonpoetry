import sys
import json
import requests

url = 'http://127.0.0.1:5000/todos'

def get():
    try:
        response = requests.get(url)

        if response.status_code == 200:
            print(response.text)
        else:
            print(f"Request failed with status code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")
    # print("get")

def max_length():
    return 69

def get_tasks(response):
    try:
        # response = requests.get(url)

        if response.status_code == 200:
            # aList = response.json
            # print(aList[0]['task'])
            
            # while max_length() >= 0:
            for i in range(max_length()+9):
                print("-", end="")
            if  json.loads(response.text)  == []:
                print("")
                for i in range(max_length() // 2):
                    print(" ", end="")
                print("empty list")
                for i in range(max_length()+9):
                   print("-", end="")
 
                return
# ----------------------------------------")
            for item in json.loads(response.text):
                cur_task = item['task']
                task_id = item['id']
                task_status = "Done" if item['status'] else "Not done"
                print("")
                print("|  " , end="")
                print(task_id, end="")
                int_length = len(str(task_id))
                status_len = len(str(task_status))
                max_length_int = 6
                max_length_status = 10
                int_pad = max_length_int - int_length
                for i in range(int_pad):
                    print(" ",end="")
                # print(int_length, end="")
                print("|  " , end="")
                print(cur_task, end="")
                # print(, end="")
                pad_status = max_length_status - status_len 
                pad = max_length() - len(cur_task)  - 20
                while (pad >= 0):
                    print(" ", end="")
                    pad = pad - 1
                print("|  ",end="")
                print(task_status , end="")
                for i in range(pad_status):
                    print(" ",end="")
                print("  |")
                for i in range(max_length()+9):
                    print("-", end="")
                # print("-----------------------------------------")
            
            print("")
        else:
            print(f"Request failed with status code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")

def post():
    data_string = input("Enter a new todo:  ")

    response = requests.post(url, json = data_string)
    get_tasks(response)

def delete_row_by_id():
    id_to_flip = input("Enter an id to remove it:  ")
    # my_json = jsonify({'id':id_to_flip})
    response = requests.delete(url, json = id_to_flip)
    get_tasks(response)

def remove_all():
    exit_status = False
    while exit_status == False:
        choice = input("Are you sure(y/n):  ")
        if choice == 'y':
            print("Removing all ...........")
            response = requests.delete(url+"/remove_all")#, json = id_to_flip)
    # my_json = jsonify({'id':id_to_flip})
            # get_tasks(response)
            print("Successfully removed ALL todos")
            exit_status = True
        elif choice == 'n':
            print("Okay, good decision")
            exit_status = True
        else:
            print("Wrong choice, choose again:")
            # delete_done()

def remove_done():
    exit_status = False
    while exit_status == False:
        choice = input("Are you sure(y/n):  ")
        if choice == 'y':
            print("Deleting done ...........")
            response = requests.delete(url+"/remove_done")#, json = id_to_flip)
    # my_json = jsonify({'id':id_to_flip})
            get_tasks(response)
            exit_status = True
        elif choice == 'n':
            print("Okay..... good decision")
            exit_status = True
        else:
            print("Wrong choice, choose again:")
            # delete_done()
#
def flip_status():
    id_to_flip = input("Enter an id to flip it's status:  ")
    # my_json = jsonify({'id':id_to_flip})
    response = requests.put(url, json = id_to_flip)
    get_tasks(response)
# "this task is added by python cli app "
    # try:

    #     if response.status_code == 200:
    #         # print("1")
    #         print(response.text)
    #     else:
    #         # print("2")
    #         print(f"Request failed with status code: {response.status_code}")
    # except requests.exceptions.RequestException as e:
    #     print(f"An error occurred: {e}")
    # # print("post")

def usage():
    print("Usage: <APP> <SUBCOMMAND> [ARGS]")
    print("SUBCOMMANDS:")
    print("     get           get todos the program")
    print("     post          post todos the program")
    # print("     tasks          get tasks in tabular format")
    print("     flip          flip status of todo of the program")
    print("     delete        delete a todo by it's id")
    print("     remove_done   delete a todo by it's whether it's done")
    print("     remove_all    delete all todos")

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage()
        print("ERROR: no subcommand is provided")
        exit(1)

    subcommand = sys.argv[1]

    response = requests.get(url)

    if subcommand == "get":
        get_tasks(response)
    elif subcommand == "post":
        post()
    # elif subcommand == "task":
    #     get_tasks(response)
    elif subcommand == "flip":
        flip_status()
    elif subcommand == "delete":
        delete_row_by_id()
    elif subcommand == "remove_done":
        remove_done()
    elif subcommand == "remove_all":
        remove_all()
    else:
        usage()
        print("ERROR: unknown subcommand %s" % (subcommand))
        exit(1)