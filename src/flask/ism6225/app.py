from flask_cors import CORS
from flask import Flask,jsonify,request
import sqlite3

app = Flask(__name__)
CORS(app)

# Connect to the database
def get_db_connection():
    conn = sqlite3.connect('database.db')
    print("Connected to database successfully")

    conn.execute('CREATE TABLE IF NOT EXISTS tasks (id INTEGER PRIMARY KEY AUTOINCREMENT ,task TEXT, status TEXT)')
    print("Created table successfully!")

    task_data = [(1,"have sex","incomplete")]
    return conn

def get_status(status):
    if status == "incomplete": 
        return False
    if status == "completed":
        return True

def post_status(status):
    if status == False:
        return "incomplete"
    if status == True: 
        return "completed"

# if x[2] == 
# jsonify list
def my_jsonify(data):
    my_json = map (lambda x: {
        'id':x[0]
        ,'task':x[1]
        ,'status': get_status(x[2])
    },data) 
    return (jsonify(list(my_json)))

# get
@app.route("/todos")
def view_tasks():
    conn = get_db_connection()
    result = conn.execute('SELECT * FROM tasks')
    data = result.fetchall()
    conn.close()
    response = my_jsonify(data)
    response.status_code = 200
    return response

# post
@app.route("/todos",methods=["POST","GET"])
def post_task_and_view():
    if request.method == 'POST':
        data = request.json
        conn = get_db_connection()
        conn.execute('INSERT INTO tasks(task,status) VALUES(?,?)'
            ,(data,post_status(False)))
                #data['status'])))
        conn.commit()
        conn.close()
        # print(data['task'])
    response = view_tasks()
    return response 

@app.route("/todos",methods=["PUT","GET"])
def edit_task_and_view():
    if request.method == 'PUT':
        data = request.json
        conn = get_db_connection()
        conn.execute('UPDATE tasks set status=? WHERE id=?'
            ,(post_status(not(data['status'])),data['id']))

# not(data['status']))
            #data['id']))
        conn.commit()
        conn.close()
        # print(data['task'])
    response = view_tasks()
    return response 
 
# conn.commit()
if __name__ == '__main__':
    print("--------------------------------------")
    print("|  Hello,                            |")
    print("|  Operations available are,         |")
    # print("commands available are,            |")
    print("|               Get                  |")
    print("|               Post                 |")
    print("|               Put                  |")
    print("--------------------------------------")
    # post,put
    app.run(debug=True)
