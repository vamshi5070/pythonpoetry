import argparse
import importlib as im

def test1():
    parser = args.ArgumentParser()
    parser.add_argument('--foo', help='foo help')
    args = parser.parse_args()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='curl app', description='A todo cli app with only backend')
    
    # parser.add_argument('--foo', help='foo of the %(prog)s program')

    # Subcommand 1: option
    greet_parser = parser.add_subparsers(title="option", description="Greet someone")
    greet_parser.add_argument("name",help="Name of the option")

    # parser.add_argument('option', help='get of the %(prog)s program')
    args = parser.parse_args()
    if hasattr(args,"name"):
        if "option" in args:
            print(f"This is {args.name}")
    # get = args.get
    
    parser.print_help()
