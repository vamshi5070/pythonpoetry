import argparse

parser = argparse.ArgumentParser(description='Process some integers')
parser.add_argument('integers', metavar='N', type=int, nargs='+',help='an integer for the accumlator')
parser.add_argument('--sum', dest='accumlate', action='store_const', const=sum, default=max,
                    help='sum the integers (default: find the max)')
args = parser.parse_args()
print(args.accumlate(args.integers))
# metavar='N', type=int, nargs='+',help='an integer for the accumlator')

